import express from 'express';

const { errors } = require('./constants');

function successResponse(res: express.Response, msg: string) {
  const data = {
    message: msg,
  };
  return res.status(200).json(data);
}

function successResponseWithData(res: express.Response, data: any) {
  return res.status(200).json(data);
}

function errorResponse(res: express.Response, errorCode: number, msg: string) {
  const data = {
    message: msg,
    code: errorCode,
  };
  return res.status(500).json(data);
}

function internError(res: express.Response, e) {
  errorResponse(
    res,
    errors.interneError.code,
    errors.interneError.message,
  );
  // eslint-disable-next-line no-console
  console.log(e);
}

function notFoundResponse(res: express.Response, msg: string) {
  const data = {
    message: msg,
  };
  return res.status(404).json(data);
}

function validationError(res: express.Response, msg: string) {
  const data = {
    message: msg,
  };
  return res.status(400).json(data);
}

function validationErrorWithData(res: express.Response, msg: string, data: JSON | any) {
  const resData = {
    message: msg,
    data,
  };
  return res.status(400).json(resData);
}

function unauthorizedResponse(res: express.Response, data: any) {
  return res.status(401).json(data);
}

export default {
  unauthorizedResponse,
  validationErrorWithData,
  validationError,
  notFoundResponse,
  errorResponse,
  internError,
  successResponseWithData,
  successResponse,
};
