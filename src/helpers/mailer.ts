const nodemailer = require('nodemailer');

exports.mailer = async (text: string, subject: string, email: string) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'mathieutercan@gmail.com',
      pass: '', // mdp a mettre en place
    },
  });
  const mailOptions = {
    from: 'Airborn <mathieutercan@gmail.com>',
    to: email,
    subject,
    text,
  };
  transporter.sendMail(mailOptions, (err, result) => {
    if (err) {
      // eslint-disable-next-line no-console
      console.log(err + result);
    }
  });
};
