import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config({ path: `.env.${process.env.NODE_ENV ? process.env.NODE_ENV : 'development'}` });
const database = process.env.DB_URL;
// eslint-disable-next-line import/prefer-default-export
const getDb = async () => {
  const client: any = await mongoose.connect(database, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
    // eslint-disable-next-line no-console
  console.log('Database connected');
  return client;
};

export default getDb;
