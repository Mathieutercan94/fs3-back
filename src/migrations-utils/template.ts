import mongoose from 'mongoose';

export const up = async () => {
  const collection = mongoose.connection.db.collection('');
  await collection.updateMany({}, { $set: { test: true } });
};

export const down = async () => {
  const collection = mongoose.connection.db.collection('users');
  await collection.updateMany({}, { $unset: { test: '' } });
};
