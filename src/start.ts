import dotenv from 'dotenv';
import connectDB from './database';
import app from './app';

dotenv.config({ path: `.env.${process.env.NODE_ENV.length ? process.env.NODE_ENV : 'development'}` });

async function main() {
  const port = process.env.PORT;

  // eslint-disable-next-line no-console
  connectDB().then(() => console.log('Connected'));

  app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log(`backend listening at http://localhost:${port}`);
  });
}
main();
