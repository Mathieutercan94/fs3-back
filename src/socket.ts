import dotenv from 'dotenv';
import UserModel from './models/User.Model';
import { User } from './models/Type';

const { Server } = require('socket.io');
const http = require('http');
const jwt = require('jsonwebtoken');
const app = require('./app');

dotenv.config({ path: `.env.${process.env.NODE_ENV.length ? process.env.NODE_ENV : 'development'}` });
const secret = process.env.JWT_SECRET;
const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    methods: ['GET', 'POST'],
  },
});
const connected = [];

io.on('connection', (socket) => {
  if (!connected.some((obj) => obj.userid === socket.handshake.auth?.id)) {
    connected.push({ userid: socket.handshake.auth?.id, socketId: socket.id });
    io.emit('refresh_connected', connected);
  }

  socket.on('disconnect', () => {
    const index = connected.findIndex((obj) => obj.userid === socket.handshake.auth?.id);
    if (index > -1) {
      connected.splice(index, 1);
    }
    io.emit('refresh_connected', connected);
  });

  socket.on('join room', async (room) => {
    try {
      const decoded = jwt.verify(socket.handshake.auth.token, secret) as { email: string };
      const user : User = await UserModel.findOne({ email: decoded.email });
      for (let i = 0; user.conversation.length > i; i += 1) {
        if (user.conversation[i] === room) {
          socket.join(room);
        }
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e);
    }
  });
  socket.on('get_connected', (callback) => {
    callback(connected);
  });

  socket.on('send invitation', (data) => {
    socket.to(data.to).emit('get new conv', data);
  });

  socket.on('chat room', (msg) => {
    io.emit('chat message', msg);
  });
});

server.listen(8081);

// eslint-disable-next-line import/prefer-default-export
export { io };
