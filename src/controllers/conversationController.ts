import { body } from 'express-validator';
import express from 'express';

import * as QueryString from 'qs';
import type { Request } from './Type';
import { checkBodyCreateConv, checkValidation } from '../middlewares/validation';
import apiResponse from '../helpers/apiResponse';
import ConversationModel from '../models/Conversation.Model';
import UserModel from '../models/User.Model';
import MessageModel from '../models/Messages.Model';
import { ConversationType, MessageType, User } from '../models/Type';
import { io } from '../socket';

const { ObjectID } = require('mongodb');
const authMiddlewares = require('../middlewares/auth');
const ConversationMiddleware = require('../middlewares/conversation');
const constantError = require('../helpers/constants');

function isId(id: string | string[] | QueryString.ParsedQs | QueryString.ParsedQs[]) {
  try {
    ObjectID(id);
    return true;
  } catch (e) {
    return false;
  }
}

async function createConversation(req: Request, res: express.Response) {
  try {
    const existedConversation: [ConversationType] = await ConversationModel.find({
      UserIds: {
        $eq: req.local.UserIds,
      },
    });
    if (existedConversation.length > 0) {
      // eslint-disable-next-line no-underscore-dangle
      return apiResponse.successResponseWithData(res, { id: existedConversation[0]._id });
    }
    const conversation = new ConversationModel(
      { UserIds: req.local.UserIds, authorsName: req.local.names },
    );
    const group = await conversation.save();
    req.local.UserIds.map(async (user) => {
      await UserModel.findOneAndUpdate({ _id: user }, {
        $push: {
          conversation: group.id,
        },
      });
      return 0;
    });
    return apiResponse.successResponseWithData(res, { id: group.id });
  } catch (e) {
    return apiResponse.internError(res, e);
  }
}

async function getAllConv(req: Request, res: express.Response) {
  try {
    const { conversation } = req.user;
    const result = await Promise.all(conversation.map(async (id) => {
      const conv: ConversationType = await ConversationModel.findOne({ _id: id }).populate('UserIds');
      const lastMessage: MessageType = await MessageModel.findOne({
        _id: conv.messageIds[conv.messageIds.length - 1],
      });
      const users = conv.UserIds.filter((user: any) => String(user.id) !== String(req.user.id));
      const tmp = users.map((e: User | any) => ({ id: e.id, name: e.name, surname: e.surname }));
      return {
        id, authors: tmp, lastMessage: lastMessage?.content,
      };
    }));
    return apiResponse.successResponseWithData(res, result);
  } catch (e) {
    return apiResponse.internError(res, e);
  }
}

async function sendMessage(req: Request, res: express.Response) {
  try {
    const { conversation, message } = req.body;
    const messageObject = new MessageModel({
      // eslint-disable-next-line no-underscore-dangle
      UserId: req.user._id,
      name: req.user.name,
      content: message,
    });
    const savedMessage: MessageType = await messageObject.save();
    await ConversationModel.findOneAndUpdate({ _id: conversation }, {
      $push: {
        messageIds: savedMessage.id,
      },
    });
    // eslint-disable-next-line no-underscore-dangle
    io.to(req.body.conversation).emit('chat message', {
      author: req.user.name, content: message, id: req.user.id, createdAt: savedMessage.createdAt,
    });
    return apiResponse.successResponse(res, 'Message saved');
  } catch (e) {
    return apiResponse.internError(res, e);
  }
}

async function getConv(req: Request, res: express.Response) {
  try {
    const idConv: string | string[] | QueryString.ParsedQs | QueryString.ParsedQs[] = req.query.id;
    let result = [];
    if (!isId(idConv)) {
      return apiResponse.errorResponse(res,
        constantError.errors.notId.code, constantError.errors.notId.message);
    }
    // @ts-ignore
    const conv: ConversationType = await ConversationModel.findOne({ _id: idConv }).populate('UserIds');
    if (!conv) {
      return apiResponse.errorResponse(res,
        constantError.errors.userNotInGroup.code, constantError.errors.userNotInGroup.message);
    }
    result = await Promise.all(conv.messageIds.map(async (id: string) => {
      const message:MessageType = await MessageModel.findOne({ _id: id });
      const { UserId, content, createdAt } = message;
      return ({ UserId, content, createdAt });
    }));
    const users = conv.UserIds.filter((a: User | any) => String(a.id) !== String(req.user.id));
    const tmp = users.map((e:User | any) => ({ id: e.id, name: e.name, surname: e.surname }));
    return apiResponse.successResponseWithData(res, { messages: result, users: tmp });
  } catch (e) {
    return apiResponse.internError(res, e);
  }
}

exports.createConv = [
  body('users').isArray().custom(checkBodyCreateConv),
  authMiddlewares.checkUser,
  authMiddlewares.checkUsers,
  ConversationMiddleware.isDuplicated,
  createConversation,
];
exports.getAllconv = [
  authMiddlewares.checkUser,
  getAllConv,
];
exports.get = [
  authMiddlewares.checkUser,
  getConv,
];
exports.sendMessages = [
  authMiddlewares.checkUser,

  body('conversation').isString(),
  body('message').isString().isLength({ min: 1 }),
  ConversationMiddleware.haveConversation,
  checkValidation,
  sendMessage,
];
