import type express from 'express';
import type { User } from '../models/Type';

export type Request = express.Request & {
  user: User,
  local: any
};
