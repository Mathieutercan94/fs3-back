import express from 'express';
import type { Request } from './Type';
import UserModel from '../models/User.Model';
import apiResponse from '../helpers/apiResponse';
import { User } from '../models/Type';

const { ObjectID } = require('mongodb');

const errorResponse = require('../helpers/constants');

async function getProfileFunction(req: Request, res: express.Response) {
  try {
    const { query, type } = req.query;
    const allowedTypes = ['email', '_id', 'name'];
    let regex;
    if (!query) {
      return apiResponse.errorResponse(res,
        errorResponse.errors.queryError.code, errorResponse.errors.queryError.message);
    }
    if (!type || !allowedTypes.includes(type.toString())) {
      return apiResponse.errorResponse(res,
        errorResponse.errors.typeError.code, errorResponse.errors.typeError.message);
    }
    if (type === '_id') {
      regex = ObjectID(query.toString());
    } else {
      regex = new RegExp(`^${query}`);
    }
    const users = await UserModel.find({ [type.toString()]: regex });
    const array = users.map((user: User) => {
      const {
        email, surname, name, id,
      } = user;
      return {
        email, surname, name, id,
      };
    });
    return apiResponse.successResponseWithData(res, array);
  } catch (e) {
    return apiResponse.internError(res, e);
  }
}

exports.getProfile = [
  getProfileFunction,
];
