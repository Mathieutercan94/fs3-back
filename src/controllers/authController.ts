/* eslint-disable no-underscore-dangle */
import bcrypt from 'bcryptjs';
import { body } from 'express-validator';
import type express from 'express';
import type { Request } from './Type';
import utility from '../helpers/utility';
import responseApi from '../helpers/apiResponse';
import UserModel from '../models/User.Model';

const authMiddlewares = require('../middlewares/auth');
const { ...validationMiddlewares } = require('../middlewares/validation');
const { validationMessages, errors } = require('../helpers/constants');

async function registerRequest(req: express.Request, res: express.Response) {
  try {
    const {
      email, password, surname, name,
    } = req.body;
    const hash = await bcrypt.hash(password, 10);
    if (!email || !password || !surname || !name) {
      return responseApi.errorResponse(res, errors.formMissing.code, errors.formMissing.message);
    }

    const user = new UserModel({
      email,
      password: hash,
      surname,
      name,
    });

    const saveUser = await user.save();
    const jwtToken = utility.generateJwtToken(saveUser._id, saveUser.email);
    return responseApi.successResponseWithData(res, {
      jwtToken,
      profile: {
        id: saveUser._id,
        email,
        name,
        surname,
      },
    });
  } catch (e) {
    return responseApi.internError(res, e);
  }
}

async function deleteUser(req: Request, res: express.Response) {
  try {
    const { email } = req.body;

    await UserModel.findOneAndDelete({ email });
    return responseApi.successResponseWithData(res, {
      message: 'Account deleted',
      email,
    });
  } catch (e) {
    return responseApi.internError(res, e);
  }
}

async function loginRequest(req: express.Request, res: express.Response) {
  try {
    const { email, password } = req.body;
    const user = await UserModel.findOne({ email });
    if (!user) {
      return responseApi.unauthorizedResponse(res, {
        code: errors.wrongCredentials.code,
        message: errors.wrongCredentials.message,
      });
    }
    const isPasswordCorrect = await user.isPasswordCorrect(password);
    if (!isPasswordCorrect) {
      return responseApi.unauthorizedResponse(res, {
        code: errors.wrongCredentials.code, message: errors.wrongCredentials.message,
      });
    }
    const jwtToken = utility.generateJwtToken(user._id, user.email);

    return responseApi.successResponseWithData(res, {
      jwtToken,
      profile: {
        id: user._id,
        email: user.email,
        name: user.name,
        surname: user.surname,
      },
    });
  } catch (e) {
    return responseApi.internError(res, e);
  }
}

exports.registerRequest = [
  body('email')
    .isLength({ min: 1 }).trim().withMessage(validationMessages.emailMissing)
    .isEmail()
    .withMessage(validationMessages.emailInvalid),
  body('password')
    .isLength({ min: 1 }).withMessage(validationMessages.passwordMissing),
  body('name').isString().notEmpty().isLength({ min: 4 }),
  body('surname').isString().notEmpty().isLength({ min: 4 }),
  validationMiddlewares.checkEmailDuplication,
  validationMiddlewares.checkValidation,
  registerRequest,
];

exports.login = [
  body('email')
    .isLength({ min: 1 }).trim().withMessage(validationMessages.emailMissing)
    .isEmail()
    .withMessage(validationMessages.emailInvalid),
  body('password')
    .isLength({ min: 1 }).withMessage(validationMessages.passwordMissing),
  validationMiddlewares.checkValidation,
  loginRequest,
];

exports.deleteUser = [
  authMiddlewares.checkUser,
  authMiddlewares.isAdmin,
  deleteUser,
];
