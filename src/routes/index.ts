import express from 'express';

const authRouter = require('./user/auth');
const profile = require('./user/profile');
const conversation = require('./conversation');

const rooter = express.Router();

const routes = [
  rooter.use('/auth/', authRouter),
  rooter.use('/profile/', profile),
  rooter.use('/conversation/', conversation),
];

export default routes;
