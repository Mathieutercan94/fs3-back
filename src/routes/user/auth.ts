import express from 'express';

const router = express.Router();
const auth = require('../../controllers/authController');

router.post('/register', auth.registerRequest);
router.post('/login', auth.login);
router.delete('/delete', auth.deleteUser);

module.exports = router;
