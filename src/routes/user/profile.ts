import express from 'express';

const profile = require('../../controllers/profileController');

const router = express.Router();
router.get('/get', profile.getProfile);

module.exports = router;
