import express from 'express';

const conversation = require('../controllers/conversationController');

const router = express.Router();

router.post('/create', conversation.createConv);
router.get('/getAll', conversation.getAllconv);
router.get('/get', conversation.get);
router.post('/sendMessage', conversation.sendMessages);

module.exports = router;
