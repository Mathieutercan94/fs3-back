import { validationResult } from 'express-validator';
import type express from 'express';
import apiResponse from '../helpers/apiResponse';
import UserModel from '../models/User.Model';

const constantMessage = require('../helpers/constants');

// eslint-disable-next-line max-len
export const checkEmailDuplication = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
  try {
    const user = await UserModel.findOne({ email: req.body.email });
    if (user) {
      return apiResponse.errorResponse(res,
        constantMessage.errors.emailDuplication.code,
        constantMessage.errors.emailDuplication.message);
    }
    return next();
  } catch (e) {
    return apiResponse.internError(res, e);
  }
};

// eslint-disable-next-line max-len
export const checkValidation = (req: express.Request, res: express.Response, next: express.NextFunction) => {
  try {
    const errorsData = validationResult(req);
    if (!errorsData.isEmpty()) {
      return apiResponse.errorResponse(
        res,
        constantMessage.errors.validationError.code,
        constantMessage.errors.validationError.message,
      );
    }
    return next();
  } catch (e) {
    return apiResponse.internError(res, e);
  }
};

export const checkBodyCreateConv = (value) => {
  if (value.length <= 1) {
    throw new Error(constantMessage.errors.numberOfUser.message);
  }
  for (let i = 0; i < value.length; i += 1) {
    if (typeof value[i] !== 'string') {
      throw new Error(constantMessage.errors.wrongBody.message);
    }
  }
  return true;
};
