// eslint-disable-next-line import/prefer-default-export
import type express from 'express';
import type { Request } from '../controllers/Type';
import apiResponse from '../helpers/apiResponse';

const errorMessages = require('../helpers/constants');

const isDuplicate = (arr, value) => arr.indexOf(value) !== arr.lastIndexOf(value);

// eslint-disable-next-line import/prefer-default-export
export const isDuplicated = (req: Request, res: express.Response, next: express.NextFunction) => {
  const usersIds = req.local.UserIds;

  if (isDuplicate(usersIds, usersIds[0])) {
    return apiResponse.errorResponse(res,
      errorMessages.errors.userAreAlreadySet.code,
      errorMessages.errors.userAreAlreadySet.message);
  }
  return next();
};

export const haveConversation = (req: Request,
  res: express.Response, next: express.NextFunction) => {
  try {
    const convIds = req.user.conversation;

    for (let i = 0; i < convIds.length; i += 1) {
      if (convIds[i] === req.body.conversation) {
        return next();
      }
    }
    return apiResponse.errorResponse(res, errorMessages.errors.userNotInGroup.code,
      errorMessages.errors.userNotInGroup.message);
  } catch (e) {
    return apiResponse.internError(res, e);
  }
};
