import jwt from 'express-jwt';
import type express from 'express';
import { validationResult } from 'express-validator';
import type { Request } from '../controllers/Type';
import responseApi from '../helpers/apiResponse';
import UserModel from '../models/User.Model';

const dotenv = require('dotenv');
const errorMessages = require('../helpers/constants');

dotenv.config({ path: `.env.${process.env.NODE_ENV.length ? process.env.NODE_ENV : 'development'}` });
const secret = process.env.JWT_SECRET;

async function checkUserExists(req: Request, res: express.Response, next: express.NextFunction) {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const user = await UserModel.findOne({ _id: req.user._id });
    if (!user) {
      return responseApi.unauthorizedResponse(res, {
        code: errorMessages.errors.userNoExist.code,
        message: errorMessages.errors.userNoExist.message,
      });
    }
    req.user = user;
    return next();
  } catch (e) {
    return responseApi.internError(res, e);
  }
}

async function isAdminFunction(req: Request, res: express.Response, next: express.NextFunction) {
  if (req.user.admin === false) {
    return responseApi.internError(res, 'This account are not an admin');
  }
  return next();
}

async function checkUserExist(id: string) {
  return UserModel.findOne({ _id: id });
}

async function checkUsersExist(req: Request, res:express.Response,
  next: express.NextFunction) {
  const { users } = req.body;
  const errors = validationResult(req);
  req.local ??= { names: [], UserIds: [] };

  if (!errors.isEmpty()) {
    return responseApi.validationErrorWithData(res, 'Validation Error', { errors, code: errorMessages.errors.validationError.code });
  }
  const promises = users.map(async (user) => {
    const userExist = await checkUserExist(user);
    if (!userExist) {
      throw new Error(errorMessages.errors.userNoExist.message);
    }
    req.local.names.push(userExist.name);
    req.local.UserIds.push(userExist.id);
  });
  try {
    await Promise.all(promises);
    return next();
  } catch (e) {
    return responseApi.errorResponse(res, errorMessages.errors.userNoExist.code,
      errorMessages.errors.userNoExist.message);
  }
}
async function checkValidationEmailFunction(req: Request, res: express.Response,
  next: express.NextFunction) {
  try {
    if (!req.user) {
      return responseApi.unauthorizedResponse(res, errorMessages.errorMessages.emailNotVerified);
    }
    return next();
  } catch (e) {
    return responseApi.internError(res, e);
  }
}
exports.checkUser = [
  jwt({ secret, algorithms: ['HS256'] }),
  checkUserExists,
];

exports.checkUsers = [
  checkUsersExist,
];

exports.checkValidationEmail = [
  checkValidationEmailFunction,
];

exports.isAdmin = [
  isAdminFunction,
];

// export default { checkUser, checkValidationEmail, isAdmin };
