import mongoose from 'mongoose';

import getDb from '../migrations-utils/db';

export const up = async () => {
  await getDb();
  const collection = await mongoose.connection.collection('conversations');
  await collection.updateMany({}, { $rename: { authors: 'authorsName' } });
};

export const down = async () => {
  await getDb();

  const collection = mongoose.connection.collection('conversations');
  await collection.updateMany({}, { $rename: { authorsName: 'authors' } });
};
