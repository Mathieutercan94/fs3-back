import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';

import apiResponse from './helpers/apiResponse';
import routes from './routes/index';
// import io from './socket';
dotenv.config({ path: '.env.development.'.concat(process.env.NODE_ENV.length ? process.env.NODE_ENV : 'development') });

const app = express();
app.disable('x-powered-by');

if (!process.env.NODE_ENV) {
  process.exit(1);
}

app.use(express.json());
app.use(cors());

app.get('/', (req: express.Request, res: express.Response) => {
  res.send('API running');
});

app.use('/api/', routes);

app.all('*', (req: express.Request, res: express.Response) => apiResponse.notFoundResponse(res, 'Not found'));

app.use((err: any, req: express.Request, res: express.Response) => {
  if (err.name === 'UnauthorizedError') {
    return apiResponse.unauthorizedResponse(res, err.message);
  }
  return apiResponse.errorResponse(res, 0, err.message);
});

export default app;
