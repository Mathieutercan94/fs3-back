import mongoose from 'mongoose';
import type { MessageType } from './Type';

const MessageSchema = new mongoose.Schema<MessageType>({
  UserId: { type: String, required: true },
  name: { type: String, required: true },
  content: { type: String, required: true },
  createdAt: { type: Date, required: true, default: Date.now },
});

const MessageModel = mongoose.model('Messages', MessageSchema);
export default MessageModel;
