import mongoose from 'mongoose';
import type { ConversationType } from './Type';

const ConversationSchema = new mongoose.Schema<ConversationType>({
  UserIds: { type: [mongoose.Schema.Types.ObjectId], required: true, ref: 'User' },
  messageIds: { type: [String], required: false },
  authorsName: { type: [String], required: true },
  createdAt: { type: Date, required: true, default: Date.now },
});

const ConversationModel = mongoose.model('Conversation', ConversationSchema);
export default ConversationModel;
