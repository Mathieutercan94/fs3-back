export type User = {
  email: string,
  password: string,
  _id: string,
  id: string,
  banned: boolean,
  verifiedEmail: boolean,
  name: string,
  avatar: string,
  surname: string,
  theme: boolean,
  conversation: [string],
  admin: boolean,
  language: number,
  createdAt: Date,
  updatedAt: Date,
};

export type ConfirmEmailType = {
  UserId: string,
  email: string,
  uuid: string,
  createdAt: Date,
};

export type MessageType = {
  UserId: string,
  id: string,
  content: string,
  name: string,
  createdAt: Date,
};

export type ConversationType = {
  UserIds: [string],
  _id: string,
  id: string,
  messageIds:[string]
  createdAt: Date,
  authorsName: [string]
};
