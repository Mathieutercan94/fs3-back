# FS3-back

## Getting started
## Instalation: 
- npm install
- installer docker
##L'executer en development:
- Cree un .env.development
- npm run start (cette commande va executer une commande docker)

##L'executer en production:
    Cree un .env.production
    npm run build

## Executer esLint: 
    npm run lint

## Cree un fichier de migration:
     npm run migrate:create

## Up ou down des fichiers de migration:
    npm run docker:migrate:up
    npm run docker:migrate:down

Le Back run sur le port 8080 et les socket sur le port 8081
